"use strict";

Template.posts.helpers({
    postList: function () {
        return Collections.Images.find({});
    },
        
    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function() {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true
            });
        });
    }
});