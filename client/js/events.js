"use strict";

Template.form.events({
    'submit .add-new-post': function (event) {
        event.preventDefault();
        
        var postImage = event.currentTarget.children[0].files[0];
        
        Collections.Images.insert(postImage, function (error, fileObjet) {
            if(error) {
                //to-do: inform the user that it failed
            } else {
                //to-do: insert post data right here
                //insert data: name, message, iimage Id, created by date
                //fileObjec._id
                $('.grid').masonry('reloadItems');
            }
        });
    }
});